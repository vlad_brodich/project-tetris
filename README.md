# Онлайн-гра ТЕТРІС.

Відеогра-головоломка, створена 1984 року радянським програмістом Олексієм Пажитновим з колегами.
Назву гри автор створив поєднавши грецький префікс «тетра-» зі словом «теніс».
Гра в різних інтерпретаціях існує майже для кожної ігрової консолі та операційної системи, а також для інших пристроїв: мобільних телефонів, медіаплеєрів, кишенькових комп'ютерів тощо. 

##         Пропоную вам спробувати мою версію популярної гри.
  https://game-tetris-vb.netlify.app/
  
## Опис.
Проєкт створено за допомогою JavaScript без використання бібліотек.

Гра має три рівні складності "Простий","Середній","Складний".
Рівень гри можливо обрати самостійно або він буде збільшуватись в залежності від кількості балів.

Простий:
- Швидкість падіння фігур: Один крок за 1 секунду.
- Не складні фігури.

Середній:
- Швидкість падіння фігур: Один крок за 0.6 секунди.
- Додаються більш складні фігури.

Складний:
- Швидкість падіння фігур: Один крок за 0.4 секунди.
- Більше складних фігур.


## Управління:

- 1.Старт/Пауза - 'Esc'
- 2.Рух фігури Ліворуч/Праворуч - 'Стрілка ліворуч'/ 'Стрілка праворуч'
- 3.Прискорення руху фігури вниз - 'Стрілка вниз'
- 4.Падіння фігури - 'Пробіл'
- 5.Обертання фігури - 'Стрілка вгору'


## Для перемоги в грі - потрібно набрати більше 10000 балів.
##            Приємної гри!


