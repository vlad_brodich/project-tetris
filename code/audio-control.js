import { allAudio } from "./constants.js";
import { stateGame } from "./stateGame.js";

// Функція запускає аудіо.
export function audioPlay(audio) {
  if (stateGame.audioVolume === 0) { return }
  audio.currentTime = 0;
  audio.volume = stateGame.audioVolume / 10;
  audio.play();
};

// Функція зупиняє всі аудіо.
export function audioAllStop() {
  allAudio.forEach((el) => {
    el.pause()
  })
};