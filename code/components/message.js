import { stateGame } from '../stateGame.js';
import { Image } from './image.js';

function createcCildren(context) {
  const pause = Image('gif/coffee.gif', 'message_img', 'coffee');
  const gameOver = `
  <div class="message_progress-wrapp">
    ${context === 'win' ?
    Image('gif/icon-trophy.gif', 'message_img', 'coffee') :
    ''}
    <!-- Максимальний результат -->
    <div class="message_progress">
      ${Image('svg/mountain_flag.svg', 'progress_img', 'rating star')}
      <p class="message_max-progress">${stateGame.maxProgress.progress}</p>
    </div>
    <p class="message_max-progress_date">${stateGame.maxProgress.date}</p>
    <!-- Поточний результат -->
    <div class="message_progress">
      ${Image('svg/rating-star.svg', 'progress_img', 'rating star')}
      <p class="message_naw-progress">${stateGame.progress}</p>
    </div>
  </div>`;
  if (context === 'puse') {
    return pause
  }else return gameOver
};


// Функція створення компонента повідомлення.
export function Message(message, flag) {
  return `<div class="message">
  ${flag ? createcCildren(flag) : ''}
  <h2>${message}</h2>
  </div>`;
};
