// -------- Імпорти -----------------------------
// Імпорт компонентів.
import { PlayField } from './components/play-field.js';
import { Message } from './components/message.js';
import { Title } from './components/title.js';
import { Image } from './components/image.js';

// Імпорт допоміжних функцій.
import { onKeyDown, spedInterval, startLoop, stopLoop } from './functions.js';
import { drawFigure, drawPlayField } from './draw.js';
import { audioAllStop, audioPlay } from './audio-control.js';

// Імпорт об'єкта стану гри.
import { stateGame } from './stateGame.js';

// Імпорт змінних.
import { PLAYFIELD_COLUMNS, PLAYFIELD_ROWS, gameOverAudio, paramAudio, rotateAudio, startGameAudio, winAudio } from './constants.js';

// ---------- Отримання елементів з DOM-дерева ------------

// Змінна елемету main_page.
const mainPage = document.querySelector('.main_page');
// Змінна елемету win-img.
const winImg = document.querySelector('.win');
// Змінна елемету header.
const header = document.querySelector('.header_wrap');
// Змінна елемету ігрового поля.
const rootPlay = document.querySelector('.root');
// Змінна екрану наступної фігури.
const wrapNextFig = document.querySelector('.next-figure__wrap');

// Змінна елемету max прогрес. 
const maxProgress = document.querySelector('.max-progress');
maxProgress.textContent = stateGame.maxProgress.progress;
const maxProgressAnimate = document.querySelector('.max-progress-animate-wrap');
// Змінна елемету прогрес.
const progress = document.querySelector('.progress');
progress.textContent = stateGame.progress;

const progressAnimate = document.querySelector('.progress-animate-wrap');

// Змінна панель налаштувань мобільна версія.
const settingsMob = document.querySelector('.control-panel_wrap');
const settingsBtnMob = document.querySelector('.control-panel');

// Змінна кнопки "Почати гру".
const btnStartPage = document.querySelector('.start-section_btn-start');
// Змінна кнопки "Почати гру".
const btnHome = document.querySelector('.btn-home');
// Змінна кнопки старт.
const btnReStart = document.querySelector('.btn-restart');
// Змінна кнопки рестарт.
const btnStart = document.querySelector('.btn-start');
// Змінна кнопок рівеня гри.
const levelBtns = document.querySelectorAll('.btn-level');
const panelLevel = document.querySelector('.panel_level');
// Змінна кнопок та панелі гучності.
const decreBtn = document.querySelector('.decre');
const increBtn = document.querySelector('.incre');
const volumePanel = document.querySelector('.volume');
// Змінна кнопок керування.
const btns = document.querySelector('.btns');

// ------- Рендер основних компонентів ---------------------

// Додавання заголовку на сторінку.
header.insertAdjacentHTML('beforeend', Title());
// Додавання ігрового поля на сторінку.
rootPlay.append(PlayField(PLAYFIELD_COLUMNS, PLAYFIELD_ROWS, 'playing-field'));

// Додавання поля наступної фігури на сторінку.
wrapNextFig.append(PlayField(4, 4, 'field-next-fig')); 

// Додавання панелі гучності на сторінку.
renderVolume();

// Змінна клітинок ігрового поля.
const cells = document.querySelectorAll('.playing-field div');
// Змінна клітинок поля наступної фігури.
const nextFigСells = document.querySelectorAll('.field-next-fig div');

// ------------- Функції які працюють з DOM-деревом. ------------

// Функція відображення результатів.
function renderProgress(num,content,animate) {
	if (num === +content.textContent) { return };
	let res = num - content.textContent;
	content.textContent = num;
	if (num === 0) { return };
	animate.innerHTML = `<span class="progress-animate">+${res}</span>`;
};

// Функція відображення повідомлення програш.
function renderGameOver() {
	rootPlay.insertAdjacentHTML('beforeend', Message('Game Over', true));
	btnReStart.classList.add('active');
	audioPlay(gameOverAudio)
};

// Функція відображення повідомлення перемога.
function renderWinGame() {
	rootPlay.insertAdjacentHTML('beforeend', Message('Вітаю ви Перемогли!', 'win'));
	winImg.innerHTML = Image('gif/win.gif', 'win-img', 'win');
	setTimeout(() => {
		winImg.innerHTML = '';
	}, 2000);
	stopLoop();
	btnReStart.classList.add('active');
	audioPlay(winAudio);
};

// Функція оновлення відображення поля та фігури.
export function render() {
	cells.forEach(cell => {
		cell.removeAttribute('class');
	});
	nextFigСells.forEach(cell => {
		cell.removeAttribute('class');
	});
	drawPlayField(cells);
	drawFigure(cells, nextFigСells)
	renderProgress(stateGame.progress, progress, progressAnimate);
	renderProgress(stateGame.maxProgress.progress, maxProgress, maxProgressAnimate);
	// renderMaxProgress(stateGame.maxProgress.progress);
	if (stateGame.gameOver) { renderGameOver() };
	if (stateGame.gameWin) { renderWinGame() };
	levelBtns.forEach(el => {
		el.classList.remove('check');
		if (stateGame.level === el.dataset.id) {
			el.classList.add('check');
		}
	});
};
render();

// Функція рестарт гри.  
function restartGame() {
	stateGame.gameOver = false;
	stateGame.gameWin= false;
	stateGame.progress = 0;
	stateGame.updatePlayfield();
	stateGame.pause ? btnStart.classList.remove('active') : btnStart.classList.add('active') 
	btnStart.innerHTML = stateGame.pause
		? Image('svg/music-play.svg',"btn-start-img","play")
		: Image('svg/music-pause.svg',"btn-start-img","pause");
	btnReStart.classList.remove('active')
	const message = document.querySelector('.message');
	message && message.remove();
	audioAllStop();
	stateGame.updateFigure();
	spedInterval(); 
	startLoop();
	render();
};

// Функція обробник події старт пауза гри.
export function startPause() {
	startGameAudio.pause();
	if (stateGame.gameOver) { return }
	if (stateGame.gameWin) { return }
	if (stateGame.pause) {
		stateGame.pause = false;
		startLoop();
		btnStart.innerHTML = Image('svg/music-pause.svg', 'btn-start-img', 'pause');
		btnStart.classList.add('active');
		
		const message = document.querySelector('.message');
		message && message.remove();
	} else {
		stateGame.pause = true;
		stopLoop();
		btnStart.innerHTML = Image('svg/music-play.svg', 'btn-start-img', 'play');
		btnStart.classList.remove('active');
		rootPlay.insertAdjacentHTML('beforeend', Message('Пауза', 'puse'));
	}
};

// Функція обробник події кнопки "Почати гру".
function startGame() {
	mainPage.classList.remove('__hiden');
	mainPage.classList.add('__game');
	audioAllStop(); 
	audioPlay(startGameAudio);
};

// Функція обробник події кнопки "На головну сторінку".
function goHome() {
	mainPage.classList.remove('__game');
	mainPage.classList.add('__hiden');
	stateGame.pause = true;
	btnStart.innerHTML = Image('svg/music-play.svg',"btn-start-img","play");
	btnStart.classList.remove('active');
	audioAllStop()
};

// Рендер панелі гучності.
function renderVolume() {
	let volume = `${Image('png/volume.png', 'volume-img', 'volume')}<span>${stateGame.audioVolume*10}%</span>`;

	if (stateGame.audioVolume === 0) {
		decreBtn.disabled = true
		volume = `${Image('png/volume-off.png', 'volume-img', 'volume off')}`;
	} else decreBtn.disabled = false;
	if (stateGame.audioVolume === 10) {
		increBtn.disabled = true;
	} else increBtn.disabled = false;
	volumePanel.innerHTML = volume;
};

// Функція обробник події Відчинити/Зачинити "панель налаштувань".
function openSettings(id) {
	if (id === "open") {
		settingsMob.classList.toggle('__show');
		audioPlay(paramAudio);
	} else if (id === "close") {
		settingsMob.classList.remove('__show')
		audioPlay(paramAudio);
	}
};


// ------------ Слухачі подій.--------------------

// Скидання контекстного меню.
document.addEventListener('contextmenu', e => {
	e.preventDefault();
});

// Слухач події клік "Відчинити/Зачинити" панель налаштувань.
settingsBtnMob.addEventListener('click', (ev) => {
	if (ev.target.dataset.btn) { 
		openSettings(ev.target.dataset.btn)
	}else ev.preventDefault();
});

// Слухач події клік кнопки "Почати гру".
btnStartPage.addEventListener('click', () => { startGame() });

// Слухач події клік кнопки "На головну сторінку".
btnHome.addEventListener('click', () => { goHome() });

// Слухач події клік кнопок рівень гри.
panelLevel.addEventListener('click', ev => {
	if (ev.target.dataset.id && !stateGame.gameOver && !stateGame.gameWin) {
		stateGame.level = ev.target.dataset.id;
		spedInterval();
		render();
		audioPlay(paramAudio);
	} else ev.preventDefault();
});

// Слухач події клік кнопки зменшити рівень гучності.
decreBtn.addEventListener('click', (ev) => {
	if (!ev.target.disabled) {
		stateGame.volumeDecrease();
		renderVolume();
		audioPlay(rotateAudio);
	} else ev.preventDefault();
});

// Слухач події клік кнопки збільшити рівень гучності.
increBtn.addEventListener('click', ev => {
	if (!ev.target.disabled) {
		stateGame.volumeIncrease();
		renderVolume()
		audioPlay(rotateAudio);
	} else ev.preventDefault();
});

// Слухач події клік кнопки старт/стоп.
btnStart.addEventListener('click', () => {startPause()});

// Слухач події клік кнопки рестарт.
btnReStart.addEventListener('click', () => { restartGame() });

// Слухач події клавіатури.
document.addEventListener('keydown', ev => {
	onKeyDown(ev.code, render);
});

// Слухач події клік кнопок положення фігури.
btns.addEventListener('click', ev => {
	ev.target.dataset.id ? onKeyDown(ev.target.dataset.id, render) : ev.preventDefault();
});