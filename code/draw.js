import { FIGURES, PLAYFIELD_COLUMNS } from "./constants.js";
import { stateGame } from "./stateGame.js";
import { convertPositionToIndex } from "./util-functions.js";


// Функція зміни кольру наступної фігури.
export function drawNextFigure(nameFigure, nextFigureColor, nextFigСells) {
	const matrix = FIGURES[nameFigure];
	matrix.forEach((elRow, row) => {
		elRow.forEach((elCol, column) => {
			const cellIndex = convertPositionToIndex(row, column, 4);
			if (elCol) {
				nextFigСells[cellIndex].classList.add(nextFigureColor);
			}
		});
	});
};

// Функція зміни кольру фігури.
export function drawFigure(cells, nextFigСells) {
	const name = stateGame.figure.color;
	stateGame.figure.matrix.forEach((elRow, row) => {
		elRow.forEach((elCol, column) => {
			if (!elCol) {
				return;
			}
			// Перевірка чи гра не програна.
			if (stateGame.gameOver) {
				return;
			}
			// Змінює стилі для наступної фігури.
			drawNextFigure(stateGame.nextFigure, stateGame.nextFigColor, nextFigСells);
			// Перевірка чи фігура з'явилась на екрані.
			const cellIndex = convertPositionToIndex(
				stateGame.figure.row + row,
				stateGame.figure.column + column,
				PLAYFIELD_COLUMNS
			);
			if (cellIndex < 0) {
				return;
			}
			// Змінює стилі для поточної фігури.
			cells[cellIndex].classList.add(stateGame.figure.color);
		});
	});
};

// Функція зміни кольру ігрового поля.
export function drawPlayField(cells) {
	stateGame.playfield.forEach((elRow, row) => {
		elRow.forEach((elCol, column) => {
			const name = elCol;
			const cellIndex = convertPositionToIndex(row, column, PLAYFIELD_COLUMNS);
			cells[cellIndex].classList.add(name);
		});
	});
};
